<?php

class Fotos{

    function __construct(){

        session_start();
    }
    public function inicio(){

        if (isset($_SESSION['fotos'])&& !empty($_REQUEST['fotos'])){
            $fotos=$_SESSION['fotos'];

        }else{
        $fotos=[];
        if(empty($fotos)){
            $_SESSION['error']="No hay imagenes que mostrar";
        }
        //esta parte es para mostrar las iḿágenes de la galeria
        $ruta= opendir("imagenes/"); //marcamos la ruta donde estan guardadas las imágenes
        while($archivo = readdir($ruta)){
            $_SESSION['error']="";
            if (is_dir($archivo)) {
            //de ser un directorio no lo guarda
            } else {
              $rutaimg="imagenes/";
              $fotos[] = $archivo;
          }
      }
  }
  require('vista_galeria.php');

}//final del metodo inicio


public function subir(){

    $subido = true;
    $uploadedfile_size = $_FILES['ficheroSubido']['size'];
    //$_SESSION['error']='';
    echo $_FILES['ficheroSubido']['name'];
    if ($_FILES['ficheroSubido']['size'] > 200000) {
        $subido = false;
    }
    //aqui hace la comprobacion del tipo
    if (!($_FILES['ficheroSubido']['type'] =="image/jpeg" || $_FILES['ficheroSubido']['type'] =="image/png")) {
        $subido = false;
    }

    $file_name = $_FILES['ficheroSubido']['name'];
    $add="imagenes/$file_name";
    if ($subido) {
        if (move_uploaded_file($_FILES['ficheroSubido']['tmp_name'], $add)) {

            header('Location:?method=nueva');
        } else {
            header('Location:?method=inicio');
            echo "Error al subir el archivo";

        }
    }else {
        header('Location:?method=inicio');
    }


    require('vista_galeria.php');

}//final del método subir
public function nueva(){
    if (isset($_REQUEST['fotos']) && !empty($_REQUEST['foto'])){

        $_SESSION['fotos'][]=$_REQUEST['foto'];
    }
    header('Location:?method=inicio');

}//final metodo nueva

public function zoom(){
    $key= $_REQUEST['key'];
    $carpeta="imagenes/";
    $rutafinal="imagenes/".$key;

    require('view_detalle.php');

}//final del metodo zoom




public function delete(){

    $file=$_REQUEST['file'];
    unlink($file);

    //si la carpeta dode eliminamos no tiene permisos puede dar error, hay que darle los permisos para poder eliminar las fotos
    header('Location:?method=inicio');
}//final del metodo delete

}//final de la clase





