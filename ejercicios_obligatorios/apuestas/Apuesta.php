<?php

class Apuesta{

  function __construct(){
    session_start();
  }
  public function inicio(){
    require('log_apuesta.php');
}//final del metodo inicio

public function validar(){
  if (isset($_REQUEST['jugador']) && !empty($_REQUEST['jugador'])){
    $jugador= $_REQUEST['jugador'];
    $_SESSION['jugador']=$jugador;
    header('Location:index.php?method=home');
  }else{
    header('Location:index.php?method=inicio');
  }
  return;
}//final del metodo validar

public function home(){

  if (!isset($_SESSION['jugador'])){
    header('Location:index.php?method=inicio');
    return;
  }
  if (isset($_SESSION['apuesta'])&& empty($_REQUEST['apuesta'])){
    $apuesta=$_SESSION['apuesta'];
  }else{
    $apuesta=[];
    $_SESSION['apuesta']=$apuesta;
  }

  //miramos el numero de elementos que hay en el array para saber que tipo de apuesta es
  if(count($_SESSION['apuesta'])<6){
    $_SESSION['tipo']="Apuesta incompleta";

  }else if(count($_SESSION['apuesta'])==6){
    $_SESSION['tipo']="Apuesta sencilla";

  }else{
    $_SESSION['tipo']="Apuesta multiple";
  }

  require('vista_apuesta.php');
}//final del metodo home


public function anade(){

  $_SESSION['apuesta'][]=$_REQUEST['contador'];
  //con array_unique miramos que el numero no se repita
    $_SESSION['apuesta']= array_unique($_SESSION['apuesta']);
  header('Location:index.php?method=home');
}//final metodo anade
public function delete(){

    $key= (integer) $_REQUEST['key'];//recoge el valor como un string y no como un numero
    unset($_SESSION['apuesta'][$key]);
    header('location:?method=home');
}//final metodo delete
public function cerrar(){
    echo "Sesion finalizada";
    session_destroy();
    unset($_SESSION);
    header('Location:index.php?method=login');
}


}//final de la clase Apuesta

