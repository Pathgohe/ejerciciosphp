<?php

class Calculadora{

    function __construct(){

        session_start();
    }
    public function inicio(){
        require('vista_calcu.php');
    }


    public function operar(){
        $op1=$_REQUEST['op1'];
        $op2=$_REQUEST['op2'];
        $operador=$_REQUEST['operacion'];

        if (isset($_REQUEST['op1']) && is_numeric($_REQUEST['op1'])){
            if (isset($_REQUEST['op2']) && is_numeric($_REQUEST['op2'])){
                if(!empty($_REQUEST['op1']) || (!empty($_REQUEST['op2']))){

                    if($_REQUEST['operacion']=='+'){
                        $resultado=$op1+$op2;

                    }elseif($_REQUEST['operacion']=='-'){
                        $resultado=$op1-$op2;

                    }elseif($_REQUEST['operacion']=='*'){
                        $resultado=$op1*$op2;

                    }else{
                        $resultado=$op1/$op2;

                    }//final de las operaciones
                    require('vista_resultado.php');
                }else{
                    $_SESSION['error'] = 'te faltan los parametros ....';
                    require('vista_calcu.php');
                    unset($_SESSION['error']);
                }

            }else{

                $_SESSION['error'] = 'parámetros obligatorios y numéricos....';
                require('vista_calcu.php');
                unset($_SESSION['error']);
            }
        }else{
            $_SESSION['error'] = 'parámetros obligatorios y numéricos....';
            require('vista_calcu.php');
            unset($_SESSION['error']);

        }

}//final del metodo operar



}//final de la clase Calculadora
