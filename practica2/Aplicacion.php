<?php
/**
*
*/
class Aplicacion
{

    function __construct()
    {
        session_start();
    }

    public function login(){
        require('index_login.php');
    }

    public function mirarLog(){

        //si existe el nombre y no esta vacio, lo guarda en la variable nombre
        if(isset($_REQUEST['nombre']) && !empty($_REQUEST['nombre'])){
            $nombre= $_REQUEST['nombre'];
        }
        //si esta marcado el recordar coge la cookie y la crea
        if(isset($_REQUEST['recordar'])){
            setcookie('nombre' , $nombre);
        }else{
            setcookie('nombre' , $nombre , time()-1);
        }

        if(isset($_COOKIE['nombre'])){
            $nombre=$_COOKIE['nombre'];
        }else{
            //estamos haciendo lo mismo pero aqui no lo guardamos en cookie el otro esta guardado en cookie(es para que aparezca simplemente)
            $nombre=$_REQUEST['nombre'];
        }
        //header("Location:vista_formulario.php");
        require('vista_formulario.php');
    }
}//final de la clase
