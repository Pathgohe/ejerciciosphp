<!DOCTYPE html>
<html>
<head>
    <title>Formulario</title>
</head>
<body>
    <h1>Bienvenido <?php echo "$nombre"; ?></h1>
    <h2>Formulario a rellenar</h2>
    <form method="post" action="?method=cogeDatos">
        <p><label>Edad:</label>
        <input type="number" value="" name="edad">
        </p>
        <p>Aficiones:</p>
        <input type="checkbox" name="aficion[]" value="deporte"> Hacer deporte <br>
        <input type="checkbox" name="aficion[]" value="bailar"> Bailar <br>
        <input type="checkbox" name="aficion[]" value="lectura"> Leer <br>
        <input type="checkbox" name="aficion[]" value="cine"> Ir al cine <br>
        <input type="checkbox" name="aficion[]" value="musica"> Musica <br>
        <input type="checkbox" name="aficion[]" value="vjuegos"> Video juegos <br>
        <input type="checkbox" name="aficion[]" value="pintar"> Pintar <br>

        <p>Sexo</p>
        <input type="radio" name="sexo" value="hombre"> Hombre<br>
        <input type="radio" name="sexo" value="mujer"> Mujer<br>
        <input type="radio" name="sexo" value="otro"> Otro<br>

        <p>Deporte favorito:</p>
        <select name="deporte_fav">
            <option value="futbol">Futbol</option>
            <option value="baloncesto">Baloncesto</option>
            <option value="tenis">Tenis</option>
            <option value="hokey">Hokey</option>
        </select>
        <input type="submit" value="Enviar">
    </form>

</body>
</html>
